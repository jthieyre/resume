$(document).ready(function() {
	$('html, body').css({
		overflow: 'hidden',
		height: '100%'
	});

	// array with texts to type in typewriter
	var dataText = [ "Hello", "Welcome to my website", "I'm Jordan Thieyre"];
	
	// type one text in the typwriter
	// keeps calling itself until the text is finished
	function typeWriter(text, i, fnCallback) {
		// chekc if text isn't finished yet
		if (i < (text.length)) {
			// add next character to typewrite-box
			document.querySelector("#typewrite-box").innerHTML = text.substring(0, i+1) +'<span id="cursor" aria-hidden="true"></span>';

			// wait for a while and call this function again for next character
			setTimeout(function() {
				typeWriter(text, i + 1, fnCallback)
			}, 100);
		}
		// text finished, call callback if there is a callback function
		else if (typeof fnCallback == 'function') {
			// call callback after timeout
			setTimeout(fnCallback, 1618);
		}
	}
	// start a typewriter animation for a text in the dataText array
	function StartTextAnimation(i) {
		if (typeof dataText[i] == 'undefined'){
			setTimeout(function() {
				const target = document.querySelector("#self-introduction");
				
				target.addEventListener('transitionend', () => target.remove());
				target.style.opacity = '0';

				$('html, body').css({
					overflow: 'auto',
					height: 'auto'
				});
			}, 1618);
		}
		// check if dataText[i] exists
		if (i < dataText[i].length) {
			// text exists! start typewriter animation
			typeWriter(dataText[i], 0, function(){
				// after callback (and whole text has been animated), start next text
				StartTextAnimation(i + 1);
		 });
		}
	}
	// start the text animation
	setTimeout(function() {
		StartTextAnimation(0)
	}, 1618);
	
	$('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
		if (
			location.pathname.replace(/^\//, "") ==
				this.pathname.replace(/^\//, "") &&
			location.hostname == this.hostname
		) {
			var target = $(this.hash);
			target = target.length
				? target
				: $("[name=" + this.hash.slice(1) + "]");
			if (target.length) {
				$("html, body").animate(
					{
						scrollTop: target.offset().top,
					},
					1000,
					"easeInOutExpo"
				);
				return false;
			}
		}
	});

	// Closes responsive menu when a scroll trigger link is clicked
	$(".js-scroll-trigger").click(function () {
		$(".navbar-collapse").collapse("hide");
	});

	// Activate scrollspy to add active class to navbar items on scroll
	$("body").scrollspy({
		target: "#sideNav",
	});
});
